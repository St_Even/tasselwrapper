#!/bin/bash -l

#arguments
param=$1

#Get parameters

#Get filepaths
pipeline=$(grep "PIPELINE=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
ptKF=${pipeline}"/job/pathToKeyFile"
ptDF=${pipeline}"/job/pathToDirectory"
ptJL=${pipeline}"/job/jobList"

#Declare arrays to hold file path strings
declare -a keyAR
declare -a dirAR

#populate arrays
    #per line append location to arrays
    while read -r dir key; do

        echo "TEST: dir: [$dir]"
        echo "TEST: key: [$key]"
        
        #send first string to dir array
        if [ -e "${dir}" ]; then
            #send string to array
            echo "assigning dir"
            dirAR+=("${dir}")
        else
                echo "ERROR: expected to read a directory from job list:"
                printf "1 unexpected value: %s" ${dir}
                exit 1
        fi

        #send second string to key array
        if [ -e "${key}" ]; then
            #send string to array
            echo "assigning key"
            keyAR+=("${key}")
        else
                echo "ERROR: expected to read a file from job list:"
                printf "2 unexpected value: %s" ${key}
                exit 1
        fi
    done < "${ptJL}"
    
    
#...other tests 

#check arrays are same length
keyCount=${#keyAR[@]}
dirCount=${#dirAR[@]}
echo "TEST: key arr length: ${#keyAR[@]}"
echo "TEST: dir arr length: ${#dirAR[@]}"

if [ $keyCount -ne $dirCount ]; then
    echo "the jobList file is incomplete or the counts do not match"
    exit 1
fi   

iter=0
total=${#dirAR[@]}
#iterate through arrays
echo "TEST: entering while loop"
while (("$iter" < "$total"))
do 
    echo "TEST: iteration::$iter of n::$total"
    #store first elements
    keyStr="${keyAR[${iter}]}"
    keyStr="PATHTOKEYFILE="${keyStr}
    echo "TEST: keyStr: [$keyStr]"

    dirStr="${dirAR[${iter}]}"
    dirStr="PATHTODIRECTORY="${dirStr}
    echo "TEST: dirStr: [$dirStr]"
    
    #send strings to pathway files

    echo $keyStr > ${ptKF}
    echo $dirStr > ${ptDF}

    #call pipeline passing parameter file
    ${pipeline}/GBSDiscovery.sh ${param}
    #prep for next iteration
    
    iter=$(( $iter + 1 ))

done

exit
    
