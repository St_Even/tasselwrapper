#!/bin/bash -l
 
timeStamp=$(date +%F_%H:%M)
SECONDS=0

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo Beginning GBS production pipeline | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

#Get Parameters
name=$(grep "NAME=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
Xms=$(grep "XMS=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
Xmx=$(grep "XMX=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
enzyme=$(grep "ENZYME=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
qcStop=$(grep "QCSTOP=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
runQC=$(grep "RUNQC=" $1 | cut -d "=" -f 2 | sed "s/\r//g")

 
#Get File paths
pipeline="$(grep "PIPELINE=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"
fastQCPath="$(grep "FASTQCPATH=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"
tasselPath="$(grep "TASSELPATH=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"

ptKF="${pipeline}""/job/pathToKeyFile"
keyFile=$(grep "PATHTOKEYFILE=" "${ptKF}" | cut -d "=" -f 2 | sed "s/\r//g")

ptDF=${pipeline}"/job/pathToDirectory"
seqDir=$(grep "PATHTODIRECTORY=" "${ptDF}" | cut -d "=" -f 2 | sed "s/\r//g")

#build auxilary file paths
nameDir="${pipeline}/project/${name}"
indexDir="${pipeline}/project/${name}_index"
dbPath="${pipeline}/project/${name}.db"
fastQCDir="${nameDir}/fastQC"
imputeDir="${nameDir}/imputation"
logPath="${nameDir}/${name}_runLog.txt"
vcfPath="${nameDir}/${name}.vcf"



#...it would be nice to load conda envrionment from within script
#conda activate BRDC

#Make working directories
file=${nameDir}
#delete existing files to prevent pileups of partially completed runs
if [ -e $file ]
then
    rm -r ${file}
fi
if [ ! -e $file ]
then
    #create directory and any parents necessary
    mkdir -p $file
fi

file=${pipeline}/projectCompleted
if [ ! -e $file ]
then
    #create directory and any parents necessary
    mkdir -p $file
fi

file=${fastQCDir}
if [ ! -e $file ]
then
    mkdir -p $file
fi

file=${imputeDir}
if [ ! -e $file ]
then
    #create directory and any parents necessary
    mkdir -p $file
fi

#establish log for this run
touch $logPath
#report parameters
echo GBS Production Pipeline: timestamp::${timeStamp} | tee -a ${logPath}
echo Parameters:name::${name} | tee -a ${logPath}
echo Parameters:Xms::${Xms} | tee -a ${logPath}
echo Parameters:Xmx::${Xmx} | tee -a ${logPath}
echo Parameters:enzyme::${enzyme} | tee -a ${logPath}
echo Parameters:qcStop::${qcStop} | tee -a ${logPath}

echo Filepaths:pipeline::${pipeline} | tee -a ${logPath}
echo Filepaths:fastQCPath::${fastQCPath} | tee -a ${logPath}
echo Filepaths:tasselPath::${tasselPath} | tee -a ${logPath}
echo Filepaths:keyFile::${keyFile} | tee -a ${logPath}
echo Filepaths:seqDir::${seqDir} | tee -a ${logPath}

echo Auxillary Filepaths:nameDir::${nameDir} | tee -a ${logPath}
echo Auxillary Filepaths:fastQCDir::${fastQCDir} | tee -a ${logPath}
echo Auxillary Filepaths:indexDir::${indexDir} | tee -a ${logPath}
echo Auxillary Filepaths:imputeDir::${imputeDir} | tee -a ${logPath}
echo Auxillary Filepaths:dbPath::${dbPath} | tee -a ${logPath}
echo Auxillary Filepaths:logPath::${logPath} | tee -a ${logPath}

#move into working directory
cd ${nameDir} || exit

if [ ${runQC} == "yes" ]
then 

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: fastQC"  | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}
 
#call fastQC on reads and output as unzipped
${fastQCPath} ${seqDir}/*fastq.gz \
    -o ${fastQCDir} \
    --extract \
    2>&1 | tee -a ${logPath}

#stop for QC check
if [ ${qcStop} == "yes" ]
then
    echo ""
    echo "Please check ${fastQCDir}"
    echo "Assess the QC files"
    echo "To continue press Y"
    echo "To exit press N"
    #prompt user, and read command line argument
    read -p "Resume?: " answer
    #handle the command line argument
    while true
    do
        case $answer in
        [yY]* )
            echo "Resuming Pipeline"
            break;;
        [nN]* )
            exit;;
        * ) 
                
            echo "Enter Y or N."
            read -p "Please check ${fastQCDir}  Resume?: " answer;;
        esac
    done
fi

fi
echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - production pipeline"  | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

## ProductionSNPCallerPluginV2 - RUN PRODUCTION PIPELINE - output .vcf
${tasselPath} ${Xms} ${Xmx} -fork1 -ProductionSNPCallerPluginV2 \
    -db ${dbPath} \
    -i ${seqDir} \
    -k ${keyFile} \
    -o ${vcfPath} \
    -e ${enzyme} -kmerLength 64 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - converting vcf to hapmap"  | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

## Convert to Hapmap format
${tasselPath} ${Xms} ${Xmx} -fork1 \
    -vcf ${vcfPath} \
    -export ${name} \
    -exportType Hapmap \
    2>&1 | tee -a ${logPath}

## imputation
echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - FILLINFindHaplotypesPlugin"  | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

${tasselPath} ${Xms} ${Xmx} -fork1 -FILLINFindHaplotypesPlugin \
    -hmp ${vcfPath} \
    -o "${imputeDir}/${name}" \
    -mxDiv 0.01 \
    -mxHet 0.01 \
    -minSites 50 \
    -mxErr 0.05 \
    -hapSize 200 \
    -minPres  500 \
    -maxHap 3000 \
    -minTaxa  2 \
    -maxOutMiss 0.4 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - FILLINImputationPlugin"  | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

${tasselPath} ${Xms} ${Xmx} -fork1 -FILLINImputationPlugin \
    -hmp ${vcfPath} \
    -d "${imputeDir}/${name}" \
    -o "${name}Imputed" \
    -hapSize 8000 \
    -mxHet 0.02 \
    -mxInbErr 0.01 \
    -mxHybErr 0.003 \
    -mnTestSite 20 \
    -minMnCnt 20 \
    -mxDonH 20 \
    -accuracy false \
    -propSitesMask 0.01 \
    -depthMask 9 \
    -propDepthSitesMask 0.2 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

#Calculate elapsed time

ELAPSED="Time to run pipeline: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS %60))sec"
echo ${ELAPSED} | tee -a ${logPath}

#clean up working files
    #rename and move project folder
    cd ..
    #...remove any excess files
    
    mv ${nameDir} ${pipeline}/projectCompleted/${name}"Production"${timeStamp}
 

#...end conda from script
#conda deactivate

exit
