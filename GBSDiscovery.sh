#!/bin/bash -l

timeStamp=$(date +%F_%H:%M)
SECONDS=0

echo ""
echo "=========================================="
echo "     Beginning GBS discovery pipeline     "
echo "=========================================="
echo ""
echo "__________________________________________" | tee -a ${logPath}
echo "__________________________________________" | tee -a ${logPath}
echo "____________________░░____________________" | tee -a ${logPath}
echo "__________________░░░░░░__________________" | tee -a ${logPath}
echo "__________________░░▒▒░░__________________" | tee -a ${logPath}
echo "________________▒▒░░░░░░▒▒________________" | tee -a ${logPath}
echo "________________░░░░▒▒░░░░________________" | tee -a ${logPath}
echo "______________▒▒▒▒░░░░░░▒▒░░______________" | tee -a ${logPath}
echo "______________░░░░░░▒▒░░░░░░______________" | tee -a ${logPath}
echo "______________▒▒▒▒░░░░░░▒▒░░______________" | tee -a ${logPath}
echo "______________▒▒░░░░▒▒░░░░░░______________" | tee -a ${logPath}
echo "______________▒▒▒▒░░░░░░▒▒░░______________" | tee -a ${logPath}
echo "____________░░▒▒░░░░▒▒░░░░░░▒▒____________" | tee -a ${logPath}
echo "____________▒▒░░▒▒░░░░░░▒▒░░░░____________" | tee -a ${logPath}
echo "____________▒▒▒▒░░░░▒▒░░░░░░▒▒____________" | tee -a ${logPath}
echo "____██▒▒____▒▒▒▒▒▒░░░░░░▒▒░░░░____▓▓▒▒____" | tee -a ${logPath}
echo "______▓▓██__▒▒░░░░░░▒▒░░░░░░▒▒__▒▒▒▒______" | tee -a ${logPath}
echo "________▓▓▒▒▒▒░░▒▒░░░░░░▒▒░░░░▓▓▓▓________" | tee -a ${logPath}
echo "__░░░░__██▓▓▒▒▒▒░░░░▒▒▒▒░░░░▒▒▓▓▒▒________" | tee -a ${logPath}
echo "▓▓▓▓▓▓░░██▓▓▒▒▒▒▒▒░░░░░░▒▒░░░░██▓▓__▒▒▒▒▒▒" | tee -a ${logPath}
echo "__▓▓▓▓▒▒████▒▒░░░░░░▒▒░░░░░░▒▒▓▓██▒▒▓▓▓▓__" | tee -a ${logPath}
echo "____▓▓▓▓▒▒██▒▒▒▒▒▒░░░░░░▒▒░░░░██▒▒▓▓▓▓____" | tee -a ${logPath}
echo "______██▒▒██▒▒▒▒▒▒░░▒▒░░░░░░▒▒██▓▓██______" | tee -a ${logPath}
echo "________▓▓▒▒▒▒▒▒░░▒▒░░░░▒▒░░▒▒▒▒▓▓________" | tee -a ${logPath}
echo "________▓▓▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▒▒▒▒▒▓▓________" | tee -a ${logPath}
echo "________██▓▓▒▒▒▒▒▒░░▒▒▒▒▒▒▒▒▒▒▓▓██________" | tee -a ${logPath}
echo "__________▓▓▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒▒▒▓▓__________" | tee -a ${logPath}
echo "__________██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██__________" | tee -a ${logPath}
echo "____________▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▓▓____________" | tee -a ${logPath}
echo "____________▓▓██▓▓▒▒▒▒▒▒▓▓▓▓██____________" | tee -a ${logPath}
echo "______________▓▓██▓▓▒▒▓▓▓▓▓▓______________" | tee -a ${logPath}
echo "________________████▓▓▓▓▓▓________________" | tee -a ${logPath}
echo "__________________██▓▓██__________________" | tee -a ${logPath}
echo "__________________██████__________________" | tee -a ${logPath}
echo "__________________██████__________________" | tee -a ${logPath}
echo "____________________▓▓____________________" | tee -a ${logPath}


#Get Parameters
name=$(grep "NAME=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
Xms=$(grep "XMS=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
Xmx=$(grep "XMX=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
enzyme=$(grep "ENZYME=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
qcStop=$(grep "QCSTOP=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
threads=$(grep "THREADS=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
runQC=$(grep "RUNQC=" $1 | cut -d "=" -f 2 | sed "s/\r//g") 

#Get File paths
pipeline="$(grep "PIPELINE=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"
fastQCPath="$(grep "FASTQCPATH=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"
tasselPath="$(grep "TASSELPATH=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"
bowtiePath="$(grep "BOWTIEPATH=" $1 | cut -d "=" -f 2 | sed "s/\r//g")"
refGen="$(grep "REFGEN=" $1 | cut -d "=" -f 2 | sed "s/\r//g")""/*"

ptKF="${pipeline}""/job/pathToKeyFile"
keyFile=$(grep "PATHTOKEYFILE=" "${ptKF}" | cut -d "=" -f 2 | sed "s/\r//g")

ptDF=${pipeline}"/job/pathToDirectory"
seqDir=$(grep "PATHTODIRECTORY=" "${ptDF}" | cut -d "=" -f 2 | sed "s/\r//g")

#build auxilary file paths
nameDir="${pipeline}""/project/""${name}"
fastQCDir="${nameDir}""/fastQC"
indexDir="${pipeline}/project/${name}_index"
dbPath=${pipeline}"/project/"${name}".db"
logPath="${nameDir}/${name}_runLog.txt"
tagsPath="${nameDir}""/""${name}""_tagsForAlign.fa.gz"


#...it would be nice to load conda envrionment from within script
#conda activate BRDC

#Make working directories
file=${nameDir}
#delete existing files to prevent pileups of partially completed runs
if [ -e $file ]
then
    rm -r ${file}
fi
if [ ! -e $file ]
then
    #create directory and any parents necessary
    mkdir -p $file
fi

file=${pipeline}/projectCompleted
if [ ! -e $file ]
then
    #create directory and any parents necessary
    mkdir -p $file
fi

file=${fastQCDir}
if [ ! -e $file ]
then
    mkdir -p $file
fi

#create index dir outside project/name dir
if [ ! -e $indexDir ]
then
    mkdir -p $indexDir
fi

#establish log for this run
touch $logPath
#report parameters
echo GBS Discovery Pipeline: timestamp::${timeStamp} | tee -a ${logPath}
echo Parameters:name::${name} | tee -a ${logPath}
echo Parameters:Xms::${Xms} | tee -a ${logPath}
echo Parameters:Xmx::${Xmx} | tee -a ${logPath}
echo Parameters:enzyme::${enzyme} | tee -a ${logPath}
echo Parameters:qcStop::${qcStop} | tee -a ${logPath}
echo Parameters:threads::${threads} | tee -a ${logPath}

echo Filepaths:pipeline::${pipeline} | tee -a ${logPath}
echo Filepaths:fastQCPath::${fastQCPath} | tee -a ${logPath}
echo Filepaths:tasselPath::${tasselPath} | tee -a ${logPath}
echo Filepaths:bowtiePath::${bowtiePath} | tee -a ${logPath}
echo Filepaths:refGen::${refGen} | tee -a ${logPath}
echo Filepaths:keyFile::${keyFile} | tee -a ${logPath}
echo Filepaths:seqDir::${seqDir} | tee -a ${logPath}

echo Auxillary Filepaths:nameDir::${nameDir} | tee -a ${logPath}
echo Auxillary Filepaths:fastQCDir::${fastQCDir} | tee -a ${logPath}
echo Auxillary Filepaths:indexDir::${indexDir} | tee -a ${logPath}
echo Auxillary Filepaths:dbPath::${dbPath} | tee -a ${logPath}
echo Auxillary Filepaths:tagsPath::${tagsPath} | tee -a ${logPath}
echo Auxillary Filepaths:logPath::${logPath} | tee -a ${logPath}

#move into working directory
cd ${nameDir} || exit

if [ "${runQC}" == "yes" ]
then

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: fastQC"  | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}
 
#call fastQC on reads and output as unzipped
${fastQCPath} ${seqDir}/*fastq.gz \
    -o ${fastQCDir} \
    --extract \
    2>&1 | tee -a ${logPath}


#stop for QC check
if [ ${qcStop} == "yes" ]
then
    echo ""
    echo "Please check ${fastQCDir}"
    echo "Assess the QC files"
    echo "To continue press Y"
    echo "To exit press N"
    #prompt user, and read command line argument
    read -p "Resume?: " answer
    #handle the command line argument
    while true
    do
        case $answer in
        [yY]* )
            echo "Resuming Pipeline"
            break;;
        [nN]* )
            exit;;
        * )

            echo "Enter Y or N."
            read -p "Please check ${fastQCDir}  Resume?: " answer;;
        esac
    done
fi

fi

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - add tags to database" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

## GBSSeqToTagDBPlugin - RUN Tags to DB
#creates the database file if it does not yet exist
$tasselPath ${Xms} ${Xmx} -fork1 -GBSSeqToTagDBPlugin \
    -e ${enzyme} \
    -i ${seqDir} \
    -db ${dbPath} \
    -k ${keyFile} \
    -deleteOldData false \
    -kmerLength 64 \
    -minKmerL 20 \
    -mnQS 20 \
    -mxKmerNum 250000000 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel -export tags for allignment" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}
 
## TagExportToFastqPlugin - export Tags  
${tasselPath} -fork1 -TagExportToFastqPlugin \
    -db ${dbPath} \
    -o ${tagsPath} \
    -c 10 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}


##create bowtie index if it does not yet exist
indexFiles="${indexDir}/${name}.1.bt2l"
if [ -e "${indexFiles}" ]
then
    echo "The existing index will be used" | tee -a ${logPath}
    echo "Contents of: "${indexDir} | tee -a ${logPath}
    for f in "${indexDir}/*"
    do 
        echo $indexFiles
        echo $(basename -s $f)
    done
else
    echo "" | tee -a ${logPath}
    echo "============================" | tee -a ${logPath}
    echo "Running: Bowtie - create index of reference genome" | tee -a ${logPath}
    echo "============================" | tee -a ${logPath}
    echo "" | tee -a ${logPath}
    ${bowtiePath}-build \
        -f \
	--large-index \
	--threads ${threads} \
        ${refGen} \
        ${indexDir}/${name} \
        2>&1 | tee -a ${logPath}
fi

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Bowtie - allign tags to reference genome" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

## RUN BOWTIE
${bowtiePath} -p 20 --end-to-end \
    -x ${indexDir}/${name} \
    --threads ${threads} \
    -U ${tagsPath} \
    -S ${name}.sam >> ${logPath} \
    2>&1 | tee -a ${logPath}

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - add alligned sam files to database" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

## SAMToGBSdbPlugin - SAM to DB
${tasselPath} ${Xms} ${Xmx} -fork1 -SAMToGBSdbPlugin \
    -i ${name}.sam \
    -db ${dbPath} \
    -aProp 0.0 \
    -aLen 0 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - call SNP" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}
 
## DiscoverySNPCallerPluginV2 - RUN DISCOVERY SNP CALLER
${tasselPath} ${Xms} ${Xmx} -fork1 -DiscoverySNPCallerPluginV2 \
    -db ${dbPath} \
    -mnLCov 0.1 \
    -mnMAF 0.01 \
    -deleteOldData false \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}
 
echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - assess SNP quality" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}

## SNPQualityProfilerPlugin - RUN QUALITY PROFILER
${tasselPath} ${Xms} ${Xmx} -fork1 -SNPQualityProfilerPlugin \
    -db ${dbPath} \
    -statFile ${name}_SNPqual_stats.txt \
    -deleteOldData false \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

echo "" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "Running: Tassel - update database with SNP quality" | tee -a ${logPath}
echo "============================" | tee -a ${logPath}
echo "" | tee -a ${logPath}
 
## UpdateSNPPositionQualityPlugin - UPDATE DATABASE WITH QUALITY SCORE
${tasselPath} ${Xms} ${Xmx} -fork1 -UpdateSNPPositionQualityPlugin \
    -db ${dbPath} \
    -qsFile ${name}_SNPqual_stats.txt \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

#Calculate elapsed time

ELAPSED="Time to run pipeline: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS %60))sec"
echo ${ELAPSED} | tee -a ${logPath}

#clean up working files
    #rename and move project folder
    cd ..
    #...remove any excess files
    mv ${nameDir} ${pipeline}/projectCompleted/${name}"Discovery"${timeStamp}
 

#...end conda from script
#conda deactivate

exit
