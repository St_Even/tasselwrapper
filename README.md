

Man pages
=========

https://bitbucket.org/tasseladmin/tassel-5-source/wiki/Home

http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml

https://www.bioinformatics.babraham.ac.uk/projects/fastqc/


pipeline design
===============

https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0090346

https://wiki.wheatgenetics.k-state.edu/index.php?title=Tassel_5_GBS_v2_Pipeline_sample_script_for_calling_SNPs_using_reference_genome


Usage: 
======


[./GBSDiscovery.sh] [./job/parameters]

[./GBSDiscoveryIterator.sh] [./job/parameters]

call conda environment from outside
    conda activate <environmentName>

call with nohup for long jobs over a remote connection
    the project will not terminate when the remote session is closed
    ...nohup.out record of terinal output is created in working directory?



Contact
=======
steven.douglas.holden@gmail.com
james.tucker@canada.ca

Citations
==========
FastQC
Andrews S. (2010). FastQC: a quality control tool for high throughput sequence data. Available online at: http://www.bioinformatics.babraham.ac.uk/projects/fastqc
Bowtie2
Langmead B, Salzberg SL. Fast gapped-read alignment with Bowtie 2. Nature Methods. 2012 Mar 4;9(4):357-9. doi: 10.1038/nmeth.1923.
Tassel5 GBSv2
Glaubitz JC, Casstevens TM, Lu F, Harriman J, Elshire RJ, Sun Q, Buckler ES. (2014) TASSEL-GBS: A High Capacity Genotyping by Sequencing Analysis Pipeline. PLoS ONE 9(2): e90346
    
Installation
============

locally install bowtie, tassel, fastqc per directions 
or 
install Anaconda with a bioconda channel and include the above in the environment.

copy the GBStoSNP script directory and note the full path to its location   
    edit the parameter PIPELINE=</path/to/this/location> with the path



DIRECTORY STRUCTURE
====================

/home
    "user"
        GBStoSNP
            job
                joblist
                parameters
                pathToKeyfile
                pathToDirectory
            project
                <name>_index
                    reference genome index files
                <name>.db
            projectCompleted
                <name>Discovery<timestamp>
                    FastQC (results directory)
                    <name>.sam
                    <name>_runLog.txt
                    <name>_tagsForAlign.fa.gz
                <name>Production<timestamp>
                    FastQC (results directory)
                    imputation (processing files directory)
                    <name>.vcf (not imputed)
                    <name>_runLog.txt
                    <name>.hmp.txt (not imputed)
                    <name>imputed.hmp.txt
                    Pipeline_Testing_ReadsPerSample.log     
            GBSDiscovery.sh
            GBSDiscoveryIterator.sh
            GBSproductionSNP.sh
            GBSproductionSNPIterator.sh
            README

To run the discovery pipeline or its iterator
    the job directory must exist,
    the project and projectCompleted directory will be created when necessary. 
    the ref genome will be indexed if necessary
    the database will be created if necessary.
the index and databse are associated with the name field set in parameters
    if name is changed then these will be re indexed and must be rebuilt respectively
    after name change The discovery pipeline will need to be rerun
    if name is reverted the matching database and index will be utilized
    untested: the possibility exists of manually renaming the database when changing name field
        to avoid rerunning the entire discovery Pipeline
        to permit calling GBSproductionSNP with a certain name
to run the production pipeline or its iterator
    the job directory must exist,
    the project directory with an index and a database which matches the name parameter must exist.
    the projectCompleted directory will be created when necessary. 

User setup 
==========

Read through parameters.txt
    edit fields following example syntax
    Avoid a classic problem with this one easy trick. 
        Remember to save! the file to write changes to disk.
open pathToDirectory and pathToKeyfile documents
    specify locations according to the parameters instructions
    save!
if using an iterator script, 
    edit the job list document 
        [/home/fullPathToDirectory/ContainingReads][tab][/home/fullPathTo/keyfile.txt][newLine]
        ...list of any length
        [/home/fullPathToDirectory/ContainingReads][tab][/home/fullPathTo/keyfile.txt][newLine]
        [cursor]
    be sure that the last newline is included and cursor is below, so the last line is processed 
    save!
    the pathToKeyfile and pathToDirectory documents will be filled line by line on each run


Usage and options
=================

input data requirements
    keyfile
        a file conforming to the tassel keyfile requirements
    reads
        a directory containing the 
        ...demultiplexed
        ...format
        ...naming
    reference geonome
        a directory containing only the refgenome files 
        if bowtie indexing is interrupted, check and delete the residual index files left in the refgenome directory

this is the end of the README
the following are detailed options for the pipeline plugins 

keyfile example

Flowcell        Lane    Barcode FullSampleName
1       1       CTCG    empty
1       1       TGCA    AC Metcalfe
1       1       ACTA    AAC Synergy
...other samples...
1       1       GCGT    BM1044-166
1       1       CGAT    BM1110-013
1       1       GTAA    BM1116-239



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GBSDiscovery.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    FastQC - A high throughput sequence QC analysis tool

SYNOPSIS

	fastqc seqfile1 seqfile2 .. seqfileN

    fastqc [-o output dir] [--(no)extract] [-f fastq|bam|sam] [-c contaminant file] seqfile1 .. seqfileN

DESCRIPTION

    FastQC reads a set of sequence files and produces from each one a quality
    control report consisting of a number of different modules, each one of 
    which will help to identify a different potential type of problem in your
    data.
    
    If no files to process are specified on the command line then the program
    will start as an interactive graphical application.  If files are provided
    on the command line then the program will run with no user interaction
    required.  In this mode it is suitable for inclusion into a standardised
    analysis pipeline.
    
    The options for the program as as follows:
    
    -h --help       Print this help file and exit
    
    -v --version    Print the version of the program and exit
    
    -o --outdir     Create all output files in the specified output directory.
                    Please note that this directory must exist as the program
                    will not create it.  If this option is not set then the 
                    output file for each sequence file is created in the same
                    directory as the sequence file which was processed.
                    
    --casava        Files come from raw casava output. Files in the same sample
                    group (differing only by the group number) will be analysed
                    as a set rather than individually. Sequences with the filter
                    flag set in the header will be excluded from the analysis.
                    Files must have the same names given to them by casava
                    (including being gzipped and ending with .gz) otherwise they
                    won't be grouped together correctly.
                    
    --nano          Files come from nanopore sequences and are in fast5 format. In
                    this mode you can pass in directories to process and the program
                    will take in all fast5 files within those directories and produce
                    a single output file from the sequences found in all files.                    
                    
    --nofilter      If running with --casava then don't remove read flagged by
                    casava as poor quality when performing the QC analysis.
                   
    --extract       If set then the zipped output file will be uncompressed in
                    the same directory after it has been created.  By default
                    this option will be set if fastqc is run in non-interactive
                    mode.
                    
    -j --java       Provides the full path to the java binary you want to use to
                    launch fastqc. If not supplied then java is assumed to be in
                    your path.
                   
    --noextract     Do not uncompress the output file after creating it.  You
                    should set this option if you do not wish to uncompress
                    the output when running in non-interactive mode.
                    
    --nogroup       Disable grouping of bases for reads >50bp. All reports will
                    show data for every base in the read.  WARNING: Using this
                    option will cause fastqc to crash and burn if you use it on
                    really long reads, and your plots may end up a ridiculous size.
                    You have been warned!
                    
    --min_length    Sets an artificial lower limit on the length of the sequence
                    to be shown in the report.  As long as you set this to a value
                    greater or equal to your longest read length then this will be
                    the sequence length used to create your read groups.  This can
                    be useful for making directly comaparable statistics from 
                    datasets with somewhat variable read lengths.
                    
    -f --format     Bypasses the normal sequence file format detection and
                    forces the program to use the specified format.  Valid
                    formats are bam,sam,bam_mapped,sam_mapped and fastq
                    
    -t --threads    Specifies the number of files which can be processed
                    simultaneously.  Each thread will be allocated 250MB of
                    memory so you shouldn't run more threads than your
                    available memory will cope with, and not more than
                    6 threads on a 32 bit machine
                  
    -c              Specifies a non-default file which contains the list of
    --contaminants  contaminants to screen overrepresented sequences against.
                    The file must contain sets of named contaminants in the
                    form name[tab]sequence.  Lines prefixed with a hash will
                    be ignored.

    -a              Specifies a non-default file which contains the list of
    --adapters      adapter sequences which will be explicity searched against
                    the library. The file must contain sets of named adapters
                    in the form name[tab]sequence.  Lines prefixed with a hash
                    will be ignored.
                    
    -l              Specifies a non-default file which contains a set of criteria
    --limits        which will be used to determine the warn/error limits for the
                    various modules.  This file can also be used to selectively 
                    remove some modules from the output all together.  The format
                    needs to mirror the default limits.txt file found in the
                    Configuration folder.
                    
   -k --kmers       Specifies the length of Kmer to look for in the Kmer content
                    module. Specified Kmer length must be between 2 and 10. Default
                    length is 7 if not specified.
                    
   -q --quiet       Supress all progress messages on stdout and only report errors.
   
   -d --dir         Selects a directory to be used for temporary files written when
                    generating report images. Defaults to system temp directory if
                    not specified.







## GBSSeqToTagDBPlugin - RUN Tags to DB
$tasselPath $Xms $Xmx $fork -GBSSeqToTagDBPlugin \
    -e $enzyme \
    -i $seqDir \
    -db ${name}.db \
    -k $keyFile \
    -kmerLength 64 \
    -minKmerL 20 \
    -mnQS 20 \
    -mxKmerNum 250000000 \
    -endPlugin \
    -runfork1 >> z_pipeline.out

GBSSeqToTagDBPlugin takes fastQ files as input, identifies tags and the taxa in which they appear, and stores this data to a local database. It keeps only good reads having a barcode and a cut site and no N’s in the useful part of the sequence. It trims off the barcodes and truncates sequences that (1) have a second cut site, or (2) read into the common adapter. The parameters to this plugin are:
    -c <Min Kmer Count> : Minimum kmer count (Default: 10) Kmers that do not appear the minimum kmer count times are removed from the database. This is a total of X count across all taxa, not in each individual taxon.
    -db <Output Database File> : Output Database File (REQUIRED)
    -deleteOldData < true | false> : Indicates whether user wants to delete old database data. If the parameter is "true" or non-existent, and the output database parameter matches a file on the system, the file is deleted and a new file by that name is created when running the plugin. If the parameter has value "false", the plugin appends data to the existing database if the "db" parameter specifies a file that exists. Appending requires all taxa from the key file to match taxa currently existing in the database. An error message is thrown and processing is stopped if this is not the case. (Default: true)
    -e <Enzyme> : Enzyme used to create the GBS library if it differs from the one listed in the key file (REQUIRED)
    -i <Input Directory>: Input directory containing FASTQ files in text or gzipped text. NOTE: Directory will be searched recursively and should be written WITHOUT a slash after its name. (REQUIRED)
    -k <Key File> : Key file listing barcodes distinguishing the samples (REQUIRED)
    -mnQS <Minimum quality score> : Minimum quality score within the barcode and read length to be accepted (Default: 0)
    -kmerLength <Kmer Length> : Length of kmer to pull from fastQ sequences (Default: 64) 
        This value is used when calculating the sequence read length for kmer creation. 
        The kmerLength value must be chosen such that the longest barcode + kmerLength < read length.
    -minKmerL <Minimum Kmer Length> : Minimum Kmer Length (Default: 20) 
        After all fastQ files have been processed the stored kmers are searched for second cut sites. 
        If a second cut site is found in the kmer sequence, the sequence is truncated at that position. 
        The -minKmerL parameter is used to ensure a minimum length kmer exists after the second cut site is removed.
    -mxKmerNum <Maximum Number of Kmers> : Maximum number of Kmers stored in the database. (Default: 50000000)
    -batchSize <Number of flow cells processed simultaneously> : Number of flow cells to process simultaneously (Default: 8)
Additionally, for this and the other plugins in the GBS pipeline, you may add the -Xms and -Xmx parameters to indicate the minimum and maximum amount of memory Java should use. The default values are platform specific. What you need will depend on your machine's available memory and the size of the data you intend to process.
The minimum quality score (mnQS) and maximum kmer length (mxKmerL) parameters are used together to determine which reads to keep from the fastQ files. The quality score of each position in the sequence is compared to the user supplied minimum quality score. If the first low quality position found occurs at a position in the sequence before the specified maximum kmer length, the read is discarded. In short, all sequence positions from 0 to (barcode length + mxKmerL) must have quality scores equal to or greater than the user supplied minimum quality score to be kept.
When using the "deleteOldData" flag to retain existing data in the database: The taxa from the key files should be identical between the old and the new runs. When running for the second time, the previous taxa list is deleted and replaced by the new now. This is to prevent duplicate taxa, which would become an error in later steps. 
 
## TagExportToFastqPlugin - export Tags
$tasselPath $fork -TagExportToFastqPlugin \
    -db ${name}.db \
    -o ${name}_tagsForAlign.fa.gz \
    -c 10 \
    -endPlugin \
    -runfork1 >> z_pipeline.out

TagExportToFastqPlugin retrieves distinct tags stored in the database and reformats them to a FASTQ file that can be read by the Bowtie2 or BWA aligner program. This output file is input to the aligner, which creates a .sam file needed for calling SNPs further down in the GBS analysis pipeline.The parameters to this plugin are
    -c <Min Count>: Minimum count of reads for a tag to be output (Default: 1) 
        This is a total of X count across all taxa, not in each individual taxon.
    -db <Input DB> : Input database file with tags and taxa distribution (REQUIRED)
    -o <Output File> : Output fastq file to use as input for BWA or Bowtie2 (REQUIRED)
To help identify tags whose sequence may have been altered by the aligner, the "seqname" field of the fastQ output file 
    is populated with the tag sequence. This value is preserved as the QNAME in the .sam file created by the aligners 
    and can be used for mapping tags to the database if the tag sequence from the aligner has been altered.

##RUN BOWTIE2-BUILD INDEXER

${bowtiePath}-build -f --threads ${threads} ${refGen} ${indexDir}/${name}

Usage:

bowtie2-build [options]* <reference_in> <bt2_base>

<reference_in>
A comma-separated list of FASTA files containing the reference sequences to be aligned to, or, if -c is specified, the sequences themselves. E.g., <reference_in> might be chr1.fa,chr2.fa,chrX.fa,chrY.fa, or, if -c is specified, this might be GGTCATCCT,ACGGGTCGT,CCGTTCTATGCGGCTTA.
<bt2_base>
The basename of the index files to write. By default, bowtie2-build writes files named NAME.1.bt2, NAME.2.bt2, NAME.3.bt2, NAME.4.bt2, NAME.rev.1.bt2, and NAME.rev.2.bt2, where NAME is <bt2_base>.
Options
-f
The reference input files (specified as <reference_in>) are FASTA files (usually having extension .fa, .mfa, .fna or similar).
-c
The reference sequences are given on the command line. I.e. <reference_in> is a comma-separated list of sequences rather than a list of FASTA files.
--large-index
Force bowtie2-build to build a large index, even if the reference is less than ~ 4 billion nucleotides inlong.
-a/--noauto
Disable the default behavior whereby bowtie2-build automatically selects values for the --bmax, --dcv and --packed parameters according to available memory. Instead, user may specify values for those parameters. If memory is exhausted during indexing, an error message will be printed; it is up to the user to try new parameters.
-p/--packed
Use a packed (2-bits-per-nucleotide) representation for DNA strings. This saves memory but makes indexing 2-3 times slower. Default: off. This is configured automatically by default; use -a/--noauto to configure manually.
--bmax <int>
The maximum number of suffixes allowed in a block. Allowing more suffixes per block makes indexing faster, but increases peak memory usage. Setting this option overrides any previous setting for --bmax, or --bmaxdivn. Default (in terms of the --bmaxdivn parameter) is --bmaxdivn 4 * number of threads. This is configured automatically by default; use -a/--noauto to configure manually.
--bmaxdivn <int>
The maximum number of suffixes allowed in a block, expressed as a fraction of the length of the reference. Setting this option overrides any previous setting for --bmax, or --bmaxdivn. Default: --bmaxdivn 4 * number of threads. This is configured automatically by default; use -a/--noauto to configure manually.
--dcv <int>
Use <int> as the period for the difference-cover sample. A larger period yields less memory overhead, but may make suffix sorting slower, especially if repeats are present. Must be a power of 2 no greater than 4096. Default: 1024. This is configured automatically by default; use -a/--noauto to configure manually.
--nodc
Disable use of the difference-cover sample. Suffix sorting becomes quadratic-time in the worst case (where the worst case is an extremely repetitive reference). Default: off.
-r/--noref
Do not build the NAME.3.bt2 and NAME.4.bt2 portions of the index, which contain a bitpacked version of the reference sequences and are used for paired-end alignment.
-3/--justref
Build only the NAME.3.bt2 and NAME.4.bt2 portions of the index, which contain a bitpacked version of the reference sequences and are used for paired-end alignment.
-o/--offrate <int>
To map alignments back to positions on the reference sequences, it’s necessary to annotate (“mark”) some or all of the Burrows-Wheeler rows with their corresponding location on the genome. -o/--offrate governs how many rows get marked: the indexer will mark every 2^<int> rows. Marking more rows makes reference-position lookups faster, but requires more memory to hold the annotations at runtime. The default is 5 (every 32nd row is marked; for human genome, annotations occupy about 340 megabytes).
-t/--ftabchars <int>
The ftab is the lookup table used to calculate an initial Burrows-Wheeler range with respect to the first <int> characters of the query. A larger <int> yields a larger lookup table but faster query times. The ftab has size 4^(<int>+1) bytes. The default setting is 10 (ftab is 4MB).
--seed <int>
Use <int> as the seed for pseudo-random number generator.
--cutoff <int>
Index only the first <int> bases of the reference sequences (cumulative across sequences) and ignore the rest.
-q/--quiet
bowtie2-build is verbose by default. With this option bowtie2-build will print only error messages.
--threads <int>
    By default bowtie2-build is using only one thread. 
    Increasing the number of threads will speed up the index building considerably in most cases.
-h/--help
Print usage information and quit.
--version
Print version information and quit.

## RUN BOWTIE
$bowtiePath \
    -p 20 \
    --end-to-end \
    -x $dbPath \
    -U ${name}_tagsForAlign.fa.gz \
    -S ${name}.sam >> z_pipeline.out
bowtie2 [options]* -x <bt2-idx> {-1 <m1> -2 <m2> | -U <r> | --interleaved <i> | --sra-acc <acc> | b <bam>} -S [<sam>]

Main arguments
    -x <bt2-idx> //The basename of the index for the reference genome. The basename is the name of any of the index files up to but not including the final .1.bt2 / .rev.1.bt2 / etc. bowtie2 looks for the specified index first in the current directory, then in the directory specified in the BOWTIE2_INDEXES environment variable.
    -1 <m1> //Comma-separated list of files containing mate 1s (filename usually includes _1), e.g. -1 flyA_1.fq,flyB_1.fq. Sequences specified with this option must correspond file-for-file and read-for-read with those specified in <m2>. Reads may be a mix of different lengths. If - is specified, bowtie2 will read the mate 1s from the “standard in” or “stdin” filehandle.
    -2 <m2> //Comma-separated list of files containing mate 2s (filename usually includes _2), e.g. -2 flyA_2.fq,flyB_2.fq. Sequences specified with this option must correspond file-for-file and read-for-read with those specified in <m1>. Reads may be a mix of different lengths. If - is specified, bowtie2 will read the mate 2s from the “standard in” or “stdin” filehandle.
    -U <r> //Comma-separated list of files containing unpaired reads to be aligned, e.g. lane1.fq,lane2.fq,lane3.fq,lane4.fq. Reads may be a mix of different lengths. If - is specified, bowtie2 gets the reads from the “standard in” or “stdin” filehandle.
    --interleaved //Reads interleaved FASTQ files where the first two records (8 lines) represent a mate pair.
    --sra-acc //Reads are SRA accessions. If the accession provided cannot be found in local storage it will be fetched from the NCBI database. If you find that SRA alignments are long running please rerun your command with the -p/--threads parameter set to desired number of threads.
        NB: this option is only available if bowtie 2 is compiled with the necessary SRA libraries. See Obtaining Bowtie 2 for details.
    -b <bam> //Reads are unaligned BAM records sorted by read name. The --align-paired-reads and --preserve-tags options affect the way Bowtie 2 processes records.
    -S <sam> //File to write SAM alignments to. By default, alignments are written to the “standard out” or “stdout” filehandle (i.e. the console).
Options
Input options
    -q Reads (specified with <m1>, <m2>, <s>) are FASTQ files. FASTQ files usually have extension .fq or .fastq. FASTQ is the default format. See also: --solexa-quals and --int-quals.
    --tab5 //Each read or pair is on a single line. An unpaired read line is [name]\t[seq]\t[qual]\n. A paired-end read line is [name]\t[seq1]\t[qual1]\t[seq2]\t[qual2]\n. An input file can be a mix of unpaired and paired-end reads and Bowtie 2 recognizes each according to the number of fields, handling each as it should.
    --tab6 //Similar to --tab5 except, for paired-end reads, the second end can have a different name from the first: [name1]\t[seq1]\t[qual1]\t[name2]\t[seq2]\t[qual2]\n
    --qseq Reads //(specified with <m1>, <m2>, <s>) are QSEQ files. QSEQ files usually end in _qseq.txt. See also: --solexa-quals and --int-quals.
    -f Reads //(specified with <m1>, <m2>, <s>) are FASTA files. FASTA files usually have extension .fa, .fasta, .mfa, .fna or similar. FASTA files do not have a way of specifying quality values, so when -f is set, the result is as if --ignore-quals is also set.
    -r Reads //(specified with <m1>, <m2>, <s>) are files with one input sequence per line, without any other information (no read names, no qualities). When -r is set, the result is as if --ignore-quals is also set.
    -F k:<int>,i:<int> //Reads are substrings (k-mers) extracted from a FASTA file <s>. Specifically, for every reference sequence in FASTA file <s>, Bowtie 2 aligns the k-mers at offsets 1, 1+i, 1+2i, … until reaching the end of the reference. Each k-mer is aligned as a separate read. Quality values are set to all Is (40 on Phred scale). Each k-mer (read) is given a name like <sequence>_<offset>, where <sequence> is the name of the FASTA sequence it was drawn from and <offset> is its 0-based offset of origin with respect to the sequence. Only single k-mers, i.e. unpaired reads, can be aligned in this way.
    -c //The read sequences are given on command line. I.e. <m1>, <m2> and <singles> are comma-separated lists of reads rather than lists of read files. There is no way to specify read names or qualities, so -c also implies --ignore-quals.
    -s/--skip <int> //Skip (i.e. do not align) the first <int> reads or pairs in the input.
    -u/--qupto <int> //Align the first <int> reads or read pairs from the input (after the -s/--skip reads or pairs have been skipped), then stop. Default: no limit.
    -5/--trim5 <int> //Trim <int> bases from 5’ (left) end of each read before alignment (default: 0).
    -3/--trim3 <int> //Trim <int> bases from 3’ (right) end of each read before alignment (default: 0).
    --trim-to [3:|5:]<int> //Trim reads exceeding <int> bases. Bases will be trimmed from either the 3’ (right) or 5’ (left) end of the read. If the read end if not specified, bowtie 2 will default to trimming from the 3’ (right) end of the read. --trim-to and -3/-5 are mutually exclusive.
    --phred33 //Input qualities are ASCII chars equal to the Phred quality plus 33. This is also called the “Phred+33” encoding, which is used by the very latest Illumina pipelines.
    --phred64 //Input qualities are ASCII chars equal to the Phred quality plus 64. This is also called the “Phred+64” encoding.
    --solexa-quals //Convert input qualities from Solexa (which can be negative) to Phred (which can’t). This scheme was used in older Illumina GA Pipeline versions (prior to 1.3). Default: off.
    --int-quals //Quality values are represented in the read input file as space-separated ASCII integers, e.g., 40 40 30 40…, rather than ASCII characters, e.g., II?I…. Integers are treated as being on the Phred quality scale unless --solexa-quals is also specified. Default: off.
Preset options in --end-to-end mode
    --very-fast //Same as: -D 5 -R 1 -N 0 -L 22 -i S,0,2.50
    --fast //Same as: -D 10 -R 2 -N 0 -L 22 -i S,0,2.50
    --sensitive //Same as: -D 15 -R 2 -N 0 -L 22 -i S,1,1.15 (default in --end-to-end mode)
    --very-sensitive //Same as: -D 20 -R 3 -N 0 -L 20 -i S,1,0.50
Preset options in --local mode
    --very-fast-local //Same as: -D 5 -R 1 -N 0 -L 25 -i S,1,2.00
    --fast-local //Same as: -D 10 -R 2 -N 0 -L 22 -i S,1,1.75
    --sensitive-local //Same as: -D 15 -R 2 -N 0 -L 20 -i S,1,0.75 (default in --local mode)
    --very-sensitive-local //Same as: -D 20 -R 3 -N 0 -L 20 -i S,1,0.50
Alignment options
    -N <int> //Sets the number of mismatches to allowed in a seed alignment during multiseed alignment. Can be set to 0 or 1. Setting this higher makes alignment slower (often much slower) but increases sensitivity. Default: 0.
    -L <int> //Sets the length of the seed substrings to align during multiseed alignment. Smaller values make alignment slower but more sensitive. Default: the --sensitive preset is used by default, which sets -L to 22 and 20 in --end-to-end mode and in --local mode.
    -i <func> //Sets a function governing the interval between seed substrings to use during multiseed alignment. For instance, if the read has 30 characters, and seed length is 10, and the seed interval is 6, the seeds extracted will be:
        Read:      TAGCTACGCTCTACGCTATCATGCATAAAC
        Seed 1 fw: TAGCTACGCT
        Seed 1 rc: AGCGTAGCTA
        Seed 2 fw:       CGCTCTACGC
        Seed 2 rc:       GCGTAGAGCG
        Seed 3 fw:             ACGCTATCAT
        Seed 3 rc:             ATGATAGCGT
        Seed 4 fw:                   TCATGCATAA
        Seed 4 rc:                   TTATGCATGA
        Since it’s best to use longer intervals for longer reads, this parameter sets the interval as a function of the read length, rather than a single one-size-fits-all number. For instance, specifying -i S,1,2.5 sets the interval function f to f(x) = 1 + 2.5 * sqrt(x), where x is the read length. See also: setting function options. If the function returns a result less than 1, it is rounded up to 1. Default: the --sensitive preset is used by default, which sets -i to S,1,1.15 in --end-to-end mode to -i S,1,0.75 in --local mode.
    --n-ceil <func> //Sets a function governing the maximum number of ambiguous characters (usually Ns and/or .s) allowed in a read as a function of read length. For instance, specifying -L,0,0.15 sets the N-ceiling function f to f(x) = 0 + 0.15 * x, where x is the read length. See also: setting function options. Reads exceeding this ceiling are filtered out. Default: L,0,0.15.
    --dpad <int> //“Pads” dynamic programming problems by <int> columns on either side to allow gaps. Default: 15.
    --gbar <int> //Disallow gaps within <int> positions of the beginning or end of the read. Default: 4.
    --ignore-quals //When calculating a mismatch penalty, always consider the quality value at the mismatched position to be the highest possible, regardless of the actual value. I.e. input is treated as though all quality values are high. This is also the default behavior when the input doesn’t specify quality values (e.g. in -f, -r, or -c modes).
    --nofw/--norc //If --nofw is specified, bowtie2 will not attempt to align unpaired reads to the forward (Watson) reference strand. If --norc is specified, bowtie2 will not attempt to align unpaired reads against the reverse-complement (Crick) reference strand. In paired-end mode, --nofw and --norc pertain to the fragments; i.e. specifying --nofw causes bowtie2 to explore only those paired-end configurations corresponding to fragments from the reverse-complement (Crick) strand. Default: both strands enabled.
    --no-1mm-upfront //By default, Bowtie 2 will attempt to find either an exact or a 1-mismatch end-to-end alignment for the read before trying the multiseed heuristic. Such alignments can be found very quickly, and many short read alignments have exact or near-exact end-to-end alignments. However, this can lead to unexpected alignments when the user also sets options governing the multiseed heuristic, like -L and -N. For instance, if the user specifies -N 0 and -L equal to the length of the read, the user will be surprised to find 1-mismatch alignments reported. This option prevents Bowtie 2 from searching for 1-mismatch end-to-end alignments before using the multiseed heuristic, which leads to the expected behavior when combined with options such as -L and -N. This comes at the expense of speed.
    --end-to-end //In this mode, Bowtie 2 requires that the entire read align from one end to the other, without any trimming (or “soft clipping”) of characters from either end. The match bonus --ma always equals 0 in this mode, so all alignment scores are less than or equal to 0, and the greatest possible alignment score is 0. This is mutually exclusive with --local. --end-to-end is the default mode.
    --local //In this mode, Bowtie 2 does not require that the entire read align from one end to the other. Rather, some characters may be omitted (“soft clipped”) from the ends in order to achieve the greatest possible alignment score. The match bonus --ma is used in this mode, and the best possible alignment score is equal to the match bonus (--ma) times the length of the read. Specifying --local and one of the presets (e.g. --local --very-fast) is equivalent to specifying the local version of the preset (--very-fast-local). This is mutually exclusive with --end-to-end. --end-to-end is the default mode.
Scoring options
    --ma <int> //Sets the match bonus. In --local mode <int> is added to the alignment score for each position where a read character aligns to a reference character and the characters match. Not used in --end-to-end mode. Default: 2.
    --mp MX,MN //Sets the maximum (MX) and minimum (MN) mismatch penalties, both integers. A number less than or equal to MX and greater than or equal to MN is subtracted from the alignment score for each position where a read character aligns to a reference character, the characters do not match, and neither is an N. If --ignore-quals is specified, the number subtracted quals MX. Otherwise, the number subtracted is MN + floor( (MX-MN)(MIN(Q, 40.0)/40.0) ) where Q is the Phred quality value. Default: MX = 6, MN = 2.
    --np <int> //Sets penalty for positions where the read, reference, or both, contain an ambiguous character such as N. Default: 1.
    --rdg <int1>,<int2> //Sets the read gap open (<int1>) and extend (<int2>) penalties. A read gap of length N gets a penalty of <int1> + N * <int2>. Default: 5, 3.
    --rfg <int1>,<int2> //fSets the reference gap open (<int1>) and extend (<int2>) penalties. A reference gap of length N gets a penalty of <int1> + N * <int2>. Default: 5, 3.
    --score-min <func> //Sets a function governing the minimum alignment score needed for an alignment to be considered “valid” (i.e. good enough to report). This is a function of read length. For instance, specifying L,0,-0.6 sets the minimum-score function f to f(x) = 0 + -0.6 * x, where x is the read length. See also: setting function options. The default in --end-to-end mode is L,-0.6,-0.6 and the default in --local mode is G,20,8.
Reporting options
    -k <int> //By default, bowtie2 searches for distinct, valid alignments for each read. When it finds a valid alignment, it continues looking for alignments that are nearly as good or better. The best alignment found is reported (randomly selected from among best if tied). Information about the best alignments is used to estimate mapping quality and to set SAM optional fields, such as AS:i and XS:i.
        When -k is specified, however, bowtie2 behaves differently. Instead, it searches for at most <int> distinct, valid alignments for each read. The search terminates when it can’t find more distinct valid alignments, or when it finds <int>, whichever happens first. All alignments found are reported in descending order by alignment score. The alignment score for a paired-end alignment equals the sum of the alignment scores of the individual mates. Each reported read or pair alignment beyond the first has the SAM ‘secondary’ bit (which equals 256) set in its FLAGS field. For reads that have more than <int> distinct, valid alignments, bowtie2 does not guarantee that the <int> alignments reported are the best possible in terms of alignment score. -k is mutually exclusive with -a.
        Note: Bowtie 2 is not designed with large values for -k in mind, and when aligning reads to long, repetitive genomes large -k can be very, very slow.
    -a //Like -k but with no upper limit on number of alignments to search for. -a is mutually exclusive with -k.
        Note: Bowtie 2 is not designed with -a mode in mind, and when aligning reads to long, repetitive genomes this mode can be very, very slow.
Effort options
    -D <int> //Up to <int> consecutive seed extension attempts can “fail” before Bowtie 2 moves on, using the alignments found so far. A seed extension “fails” if it does not yield a new best or a new second-best alignment. This limit is automatically adjusted up when -k or -a are specified. Default: 15.
    -R <int> //<int> is the maximum number of times Bowtie 2 will “re-seed” reads with repetitive seeds. When “re-seeding,” Bowtie 2 simply chooses a new set of reads (same length, same number of mismatches allowed) at different offsets and searches for more alignments. A read is considered to have repetitive seeds if the total number of seed hits divided by the number of seeds that aligned at least once is greater than 300. Default: 2.
Paired-end options
    -I/--minins <int> //The minimum fragment length for valid paired-end alignments. E.g. if -I 60 is specified and a paired-end alignment consists of two 20-bp alignments in the appropriate orientation with a 20-bp gap between them, that alignment is considered valid (as long as -X is also satisfied). A 19-bp gap would not be valid in that case. If trimming options -3 or -5 are also used, the -I constraint is applied with respect to the untrimmed mates.
        The larger the difference between -I and -X, the slower Bowtie 2 will run. This is because larger differences between -I and -X require that Bowtie 2 scan a larger window to determine if a concordant alignment exists. For typical fragment length ranges (200 to 400 nucleotides), Bowtie 2 is very efficient.
        Default: 0 (essentially imposing no minimum)
    -X/--maxins <int> //The maximum fragment length for valid paired-end alignments. E.g. if -X 100 is specified and a paired-end alignment consists of two 20-bp alignments in the proper orientation with a 60-bp gap between them, that alignment is considered valid (as long as -I is also satisfied). A 61-bp gap would not be valid in that case. If trimming options -3 or -5 are also used, the -X constraint is applied with respect to the untrimmed mates, not the trimmed mates.
        The larger the difference between -I and -X, the slower Bowtie 2 will run. This is because larger differences between -I and -X require that Bowtie 2 scan a larger window to determine if a concordant alignment exists. For typical fragment length ranges (200 to 400 nucleotides), Bowtie 2 is very efficient.
        Default: 500.
    --fr/--rf/--ff //The upstream/downstream mate orientations for a valid paired-end alignment against the forward reference strand. E.g., if --fr is specified and there is a candidate paired-end alignment where mate 1 appears upstream of the reverse complement of mate 2 and the fragment length constraints (-I and -X) are met, that alignment is valid. Also, if mate 2 appears upstream of the reverse complement of mate 1 and all other constraints are met, that too is valid. --rf likewise requires that an upstream mate1 be reverse-complemented and a downstream mate2 be forward-oriented. --ff requires both an upstream mate 1 and a downstream mate 2 to be forward-oriented. Default: --fr (appropriate for Illumina’s Paired-end Sequencing Assay).
    --no-mixed //By default, when bowtie2 cannot find a concordant or discordant alignment for a pair, it then tries to find alignments for the individual mates. This option disables that behavior.
    --no-discordant //By default, bowtie2 looks for discordant alignments if it cannot find any concordant alignments. A discordant alignment is an alignment where both mates align uniquely, but that does not satisfy the paired-end constraints (--fr/--rf/--ff, -I, -X). This option disables that behavior.
    --dovetail //If the mates “dovetail”, that is if one mate alignment extends past the beginning of the other such that the wrong mate begins upstream, consider that to be concordant. See also: Mates can overlap, contain or dovetail each other. Default: mates cannot dovetail in a concordant alignment.
    --no-contain //If one mate alignment contains the other, consider that to be non-concordant. See also: Mates can overlap, contain or dovetail each other. Default: a mate can contain the other in a concordant alignment.
    --no-overlap //If one mate alignment overlaps the other at all, consider that to be non-concordant. See also: Mates can overlap, contain or dovetail each other. Default: mates can overlap in a concordant alignment.
BAM options
    --align-paired-reads //Bowtie 2 will, by default, attempt to align unpaired BAM reads. Use this option to align paired-end reads instead.
    --preserve-tags //Preserve tags from the original BAM record by appending them to the end of the corresponding Bowtie 2 SAM output.
Output options
    -t/--time //Print the wall-clock time required to load the index files and align the reads. This is printed to the “standard error” (“stderr”) filehandle. Default: off.
    
    --un <path>
    --un-gz <path>
    --un-bz2 <path>
    --un-lz4 <path> //Write unpaired reads that fail to align to file at <path>. These reads correspond to the SAM records with the FLAGS 0x4 bit set and neither the 0x40 nor 0x80 bits set. If --un-gz is specified, output will be gzip compressed. If --un-bz2 or --un-lz4 is specified, output will be bzip2 or lz4 compressed. Reads written in this way will appear exactly as they did in the input file, without any modification (same sequence, same name, same quality string, same quality encoding). Reads will not necessarily appear in the same order as they did in the input.

    --al <path>
    --al-gz <path>
    --al-bz2 <path>
    --al-lz4 <path> //Write unpaired reads that align at least once to file at <path>. These reads correspond to the SAM records with the FLAGS 0x4, 0x40, and 0x80 bits unset. If --al-gz is specified, output will be gzip compressed. If --al-bz2 is specified, output will be bzip2 compressed. Similarly if --al-lz4 is specified, output will be lz4 compressed. Reads written in this way will appear exactly as they did in the input file, without any modification (same sequence, same name, same quality string, same quality encoding). Reads will not necessarily appear in the same order as they did in the input.

    --un-conc <path>
    --un-conc-gz <path>
    --un-conc-bz2 <path>
    --un-conc-lz4 <path> //Write paired-end reads that fail to align concordantly to file(s) at <path>. These reads correspond to the SAM records with the FLAGS 0x4 bit set and either the 0x40 or 0x80 bit set (depending on whether it’s mate #1 or #2). .1 and .2 strings are added to the filename to distinguish which file contains mate #1 and mate #2. If a percent symbol, %, is used in <path>, the percent symbol is replaced with 1 or 2 to make the per-mate filenames. Otherwise, .1 or .2 are added before the final dot in <path> to make the per-mate filenames. Reads written in this way will appear exactly as they did in the input files, without any modification (same sequence, same name, same quality string, same quality encoding). Reads will not necessarily appear in the same order as they did in the inputs.

    --al-conc <path>
    --al-conc-gz <path>
    --al-conc-bz2 <path>
    --al-conc-lz4 <path> //Write paired-end reads that align concordantly at least once to file(s) at <path>. These reads correspond to the SAM records with the FLAGS 0x4 bit unset and either the 0x40 or 0x80 bit set (depending on whether it’s mate #1 or #2). .1 and .2 strings are added to the filename to distinguish which file contains mate #1 and mate #2. If a percent symbol, %, is used in <path>, the percent symbol is replaced with 1 or 2 to make the per-mate filenames. Otherwise, .1 or .2 are added before the final dot in <path> to make the per-mate filenames. Reads written in this way will appear exactly as they did in the input files, without any modification (same sequence, same name, same quality string, same quality encoding). Reads will not necessarily appear in the same order as they did in the inputs.

    --quiet //Print nothing besides alignments and serious errors.
    --met-file <path> //Write bowtie2 metrics to file <path>. Having alignment metric can be useful for debugging certain problems, especially performance issues. See also: --met. Default: metrics disabled.
    --met-stderr <path> //Write bowtie2 metrics to the “standard error” (“stderr”) filehandle. This is not mutually exclusive with --met-file. Having alignment metric can be useful for debugging certain problems, especially performance issues. See also: --met. Default: metrics disabled.
    --met <int> //Write a new bowtie2 metrics record every <int> seconds. Only matters if either --met-stderr or --met-file are specified. Default: 1.
SAM options
    --no-unal //Suppress SAM records for reads that failed to align.
    --no-hd //Suppress SAM header lines (starting with @).
    --no-sq //Suppress @SQ SAM header lines.
    --rg-id <text> //Set the read group ID to <text>. This causes the SAM @RG header line to be printed, with <text> as the value associated with the ID: tag. It also causes the RG:Z: extra field to be attached to each SAM output record, with value set to <text>.
    --rg <text> //Add <text> (usually of the form TAG:VAL, e.g. SM:Pool1) as a field on the @RG header line. Note: in order for the @RG line to appear, --rg-id must also be specified. This is because the ID tag is required by the SAM Spec. Specify --rg multiple times to set multiple fields. See the SAM Spec for details about what fields are legal.
    --omit-sec-seq //When printing secondary alignments, Bowtie 2 by default will write out the SEQ and QUAL strings. Specifying this option causes Bowtie 2 to print an asterisk in those fields instead.
    --soft-clipped-unmapped-tlen //Consider soft-clipped bases unmapped when calculating TLEN. Only available in --local mode.
    --sam-no-qname-trunc //Suppress standard behavior of truncating readname at first whitespace at the expense of generating non-standard SAM
    --xeq //Use '='/'X', instead of 'M', to specify matches/mismatches in SAM record
Performance options
    -o/--offrate <int> //Override the offrate of the index with <int>. If <int> is greater than the offrate used to build the index, then some row markings are discarded when the index is read into memory. This reduces the memory footprint of the aligner but requires more time to calculate text offsets. <int> must be greater than the value used to build the index.
    -p/--threads NTHREADS //Launch NTHREADS parallel search threads (default: 1). Threads will run on separate processors/cores and synchronize when parsing reads and outputting alignments. Searching for alignments is highly parallel, and speedup is close to linear. Increasing -p increases Bowtie 2’s memory footprint. E.g. when aligning to a human genome index, increasing -p from 1 to 8 increases the memory footprint by a few hundred megabytes. This option is only available if bowtie is linked with the pthreads library (i.e. if BOWTIE_PTHREADS=0 is not specified at build time).
    --reorder //Guarantees that output SAM records are printed in an order corresponding to the order of the reads in the original input file, even when -p is set greater than 1. Specifying --reorder and setting -p greater than 1 causes Bowtie 2 to run somewhat slower and use somewhat more memory than if --reorder were not specified. Has no effect if -p is set to 1, since output order will naturally correspond to input order in that case.
    --mm //Use memory-mapped I/O to load the index, rather than typical file I/O. Memory-mapping allows many concurrent bowtie processes on the same computer to share the same memory image of the index (i.e. you pay the memory overhead just once). This facilitates memory-efficient parallelization of bowtie in situations where using -p is not possible or not preferable.
Other options
    --qc-filter //Filter out reads for which the QSEQ filter field is non-zero. Only has an effect when read format is --qseq. Default: off.
    --seed <int> //Use <int> as the seed for pseudo-random number generator. Default: 0.
    --non-deterministic //Normally, Bowtie 2 re-initializes its pseudo-random generator for each read. It seeds the generator with a number derived from (a) the read name, (b) the nucleotide sequence, (c) the quality sequence, (d) the value of the --seed option. This means that if two reads are identical (same name, same nucleotides, same qualities) Bowtie 2 will find and report the same alignment(s) for both, even if there was ambiguity. When --non-deterministic is specified, Bowtie 2 re-initializes its pseudo-random generator for each read using the current time. This means that Bowtie 2 will not necessarily report the same alignment for two identical reads. This is counter-intuitive for some users, but might be more appropriate in situations where the input consists of many identical reads.
    --version //Print version information and quit.
    -h/--help //Print usage information and quit. 
 
## SAMToGBSdbPlugin - SAM to DB
$tasselPath $Xms $Xmx $fork -SAMToGBSdbPlugin \
    -i ${name}.sam \
    -db ${name}.db \
    -aProp 0.0 \
    -aLen 0 \
    -endPlugin \
    -runfork1 >> z_pipeline.out

SAMToGBSdbPlugin reads a SAM file to determine the potential positions of Tags against the reference genome. The plugin updates the current database with information on tag cut positions. It will throw an error if there are tags found in the SAM file that can not be matched to tags in the database.
The parameters to this plugin are:
    -aLen< Minimum length of aligned base pair to store the SAM entry> (Default is 0)
    -aProp <Minimum proportion of sequence that must align to store the SAM entry> (Default is 0)
    -i <SAM Input file> (REQUIRED)
    -db <GBS DB file> (REQUIRED)
    -minMAPQ < Minimum value of MAPQ field to store the SAM entry > (Default is 0)
    -deleteOldData <true | false> If value is "true", database tables populated from this step and subsequent steps of the GBSv2 pipeline are cleared. This removes all data related to tag cur positions, SNPs and SNPQuality and allows for re-running the pipeline with a new data from this step. (Default: true)
Both the minAlignmentLength and minAlignProportion code check the MD value (regex String for mismatching positions) for number of positions in the sequence that match the reference. The CIGAR value is used if MD is not available. This number is used to determine if the tag meets the minAlignmentLength/Proportion specified by the user. If they do not, the tag will be stored in the database without this corresponding position.
A high value for Minimum length aligned or minimum proportion aligned will result in fewer cut positions stored against tags in the database. A default of 0 should be used for both if the desire is to have all tags/positions included.
The minMAPQ parameter can be used to limit filter low quality SNPs before storing in the database.
If tags exist in the .sam file which are not found in the database, the data is considered inconsistent and no alignments are added to the database tables. 
 
## DiscoverySNPCallerPluginV2 - RUN DISCOVERY SNP CALLER
$tasselPath $Xms $Xmx $fork -DiscoverySNPCallerPluginV2 \
    -db ${name}.db \
    -mnLCov 0.1 \
    -mnMAF 0.01 \
    -deleteOldData true \
    -endPlugin \
    -runfork1 >> z_pipeline.out

DiscoverySNPCallerPluginV2 takes a GBSv2 database file as input and identifies SNPs from the aligned tags. Tags positioned at the same physical location are aligned against one another, SNPs are called from the aligned tags, and the SNP position and allele data are written to the database.
The parameters to this plugin are:
    -callBiSNPsWGap <true | false> : Include sites where the third allele is a GAP (mutually exclusive with inclGaps) (Default: false) This option has not yet been implemented.
    -db <Input GBS Database> : Input Database file if using SQLite (REQUIRED)
    -gapAlignRatio <Gap Alignment Threshold> : Gap alignment threshold ratio of indel contrasts to non indel contrasts: IC/(IC + NC). Tags will be excluded from any loci that has a tag with a gap ratio that exceeds the threshold.
    -inclGaps <true | false > : Include sites where major or minor allele is a GAP (Default: false) This option has not yet been implemented.
    -inclRare <true | false> : Include the rare alleles at site (3 or 4th states) (Default: false) This option has not yet been implemented.
    -maxTagsCutSite <Max tags per cut site position> : Maximum number of tags allowed per cut site when aligning tags . All cut site positions and their tags are stored in the database, but alignment of tags at a particular cut site position only occurs when the number of tags at this position is equal to or less than maxTagsPerCutSite. This guards against software degradation when a position has hundreds or thousands of associated tags. (Default: 64)
    -mnLCov <Min Locus Coverage> : Minimum locus coverage (proportion of Taxa with a genotype) (Default: 0.1)
    -mnMAF <Min Minor Allele Freq> : Minimum minor allele frequency (Default: 0.01)
    -ref <Reference Genome File> : Path to reference genome in fasta format. Ensures that a tag from the reference genome is always included when the tags at a locus are aligned against each other to call SNPs. (Default: Don’t use reference genome)
    -sC <Start Chromosome> : Start Chromosome: If missing, processing starts with the first chromosome (lexicographically) in the database.
    -eC <End Chromosome> : End Chromosome : If missing, plugin processing ends with the last chromosome (lexicographically) in the database.
    -deleteOldData <true | false> : Whether to delete old SNP data from the data bases. If true, all data base tables previously populated from the DiscoverySNPCallerPluginV2 and later steps in the GBSv2 pipeline is deleted. This allows for calling new SNPs with different pipeline parameters. (Default: true)
DiscoverySNPCallerPluginV2 now takes string values for start and end chromosome. The software translates the chromosome parameter value to upper case, strips off a leading "CHR" or "CHROMOSOME", and stores the remaining value up to the first space as the chromosome name. Chromosomes that parse to an integer are compared as ints when determining order. Otherwise chromosomes are compared lexicographically. 
 
## SNPQualityProfilerPlugin - RUN QUALITY PROFILER
$tasselPath $Xms $Xmx $fork -SNPQualityProfilerPlugin \
    -db ${name}.db \
    -statFile ${name}_SNPqual_stats.txt \
    -endPlugin \
    -runfork1 >> z_pipeline.out

This plugin scores all discovered SNPs for various coverage, depth and genotypic statistics for a given set of taxa. If no taxa are specified, the plugin will score all taxa currently stored in the data base. If no taxa file is specified, the plugin uses the taxa stored in the database.
The parameters to this plugin are:
    -db <Output Database file> : Name of output file (e.g. GBSv2.db) (REQUIRED)
    -deleteOldData <true | false> : Delete existing SNP quality data from db tables. If true, SNP quality data is cleared from the snpQuality table in the database before adding new data. (Default: true)
    -taxa <Taxa sublist file> : Name of taxa list input file in taxa list format. If no taxa file is specified, all taxa from the master list are used to calculate the quality metrics. By using taxa sublist files, the user can store quality metrics for different subsets of the taxa concurrently in the data base.
    -tname <Taxa set name > : Name of taxa set for database. This allows the user to identify quality metrics that were run for a specific set of taxa. The tname parameter should be used if a subset of taxa is specified in a taxa list file. This allows for identifying the quality metrics calculated for a specific subset of taxa. The default name used when none is supplied is "ALL".
    -statFile <Quality information output name > : Name of the output file containing the quality statistics in a tab delimited format.
A description of the data contained in the output file:
    aveDepth: Average taxon read depth at SNP.
    minorDepthProp – the percentage of total depth consisting of minor allele 1
    minorDepthProp2 – the percentage of total depth consisting of minor allele 2
    gapDepthProp – percentage of the total depth that represents a gap (allele = GAP_ALLELE, gapDepth/total_depths)
    propCovered – proportion of taxa with depth > 0 at SNP
    propCovered2 – proportion of taxa with depth > 1 at SNP
    taxaCntWithMinorAlleleGE2 – number of taxa containing the minor allele 1 at depth > 1
If there is more than one allele, the following output is included:
    genotypeCnt: total number of taxa with a depth > 1
    minorAlleleFreqGE2 – minor allele frequency calculated from the taxa with depth > 1
    hetFreq_DGE2 – number of taxa with a depth > 1 that appear heterozygous
    inbredF_DGE2 – inbreeding coefficient for tax with depth > 1, calculated as: 1 - (proportion hets/expected heterozygosity)

 
## UpdateSNPPositionQualityPlugin - UPDATE DATABASE WITH QUALITY SCORE
$tasselPath $Xms $Xmx $fork -UpdateSNPPositionQualityPlugin \
    -db ${name}.db \
    -qsFile ${name}_SNPqual_stats.txt \
    -endPlugin \
    -runfork1 >> z_pipeline.out

UpdateSNPPositionQualityPlugin reads a quality score file to obtain quality score data for positions stored in the snpposition table. The quality score file is a user created file that supplies quality scores for SNP positions. It is up to the user to determine what values should be associated with each SNP. SNPQualityProfilerPlugin output provides data for this analysis, or the user may base quality scores on other data of his/her choice.
The parameters to this plugin are:
    -db< Input Database> : Input database file with SNP positions stored (REQUIRED)
    -qsFile <Quality Score File> : A tab delimited txt file containing headers CHROM( String), POS (Integer) and QUALITYSCORE(Float) for filtering. (REQUIRED)
The quality scores are float values and may be any value the user desires. These values may be used in the ProductionSNPCallerPluginV2 plugin to pull only positions whose value exceed a specified value.
 


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GBStoSNPproduction.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## ProductionSNPCallerPluginV2 - RUN PRODUCTION PIPELINE - output .vcf
$tasselPath $Xms $Xmx $fork -ProductionSNPCallerPluginV2 \
    -db ${name}.db \
    -i $seqDir \
    -k $keyFile \
    -o ${name}.vcf \
    -e ${enzyme} \
    -kmerLength 64 \
    -endPlugin \
    -runfork1 >> z_pipeline.out

This plugin converts data from fastq and keyfile to genotypes, then adds these to a genotype file in VCF or HDF5 format. VCF is the default output. An HDF5 file may be requested by using the suffix ".h5" on the file used in the output file parameter. Merging of samples to the same HDF5 output file may be accomplished by using the –ko option described below.
The parameters to this plugin are:
    -batchSize <Batch Size> : Number of flow cells to process simultaneously. (Default: 8)
    -d <Max Divergence> : Maximum divergence (edit distance) between new read and previously mapped read (Default: 0 = perfect matches only) (Default: 0)
    -db <Input GBS Database> : Input Database file if using SQLite (REQUIRED)
    -e <Enzyme> : Enzyme used to create the GBS library (REGQUIRED)
    -eR <Ave Seq Error Rate> : Average sequencing error rate per base (used to decide between heterozygous and homozygous calls) (Default: 0.01)
    -i <Input Directory> : Input directory containing fastq AND/OR qseq files (REQUIRED)
    -k <Key File> : Key file listing barcodes distinguishing the sample (REQUIRED)
    -ko <true | false> : Keep HDF5 genotypes file open for future runs that add more taxa or more depth (Default: false)
    -kmerLength <Length of Kmer> : Lemgth of kmers to grab from fastQ sequences. This value should match the kmerLength parameter value used in the GBSSeqToTagDBPlugin step of the pipeline. Bad values may be stored in the HDF5 file if these values are inconsistent. (Default: 64)
    -minPosQS < Minimum Quality Score> : The minimum quality score a SNP position must have to be output to the HDF5 file. The quality scores are loaded into the database via the tab-delimited file read in from the UpdateSNPPositionQualityPlugin. A value of 0 indicates all positions should be processed. (Default: 0)
    -mnQS < Minimum Quality Score> : The minimum quality score within the barcode and read length for a position to be accepted. This filters the read values from the fastQ files. (Default: 0)
    -do <true | false> : depth output: write depths to the output HDF5 genotypes file (Default: true)
    -o <Output Genotypes File> : Output (target) genotypes file to which is added new genotypes. VCF format is the default. if the file specified has suffix ".h5" output will be to an HDF5 file. (REQUIRED)

 
## Convert to Hapmap format
$tasselPath $Xms $Xmx $fork \
    -vcf ${name}.vcf \
    -export ${name} \
    -exportType Hapmap


{tasselPath} ${Xms} ${Xmx} -fork1 -FILLINFindHaplotypesPlugin \
    -hmp <Target file> \
    -o "${imputeDir}/${name}" \
    -mxDiv 0.01 \
    -mxHet 0.01 \
    -minSites 50 \
    -mxErr 0.05 \
    -hapSize 8192 \
    -minPres  500 \
    -maxHap 3000 \
    -minTaxa  2 \
    -maxOutMiss 0.4 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}

Usage:

FILLINFindHaplotypesPlugin <options>

    -hmp <Target file> : Input genotypes to generate haplotypes from. Usually best to use all available samples from a species. Accepts all file types supported by TASSEL5. (required)
    -o <Donor dir/file basename> 
        Output file directory name, or new directory path; Directory will be created, if doesn't exist. Outfiles will be placed in the directory and given the same name and appended with the substring '.gc#s#.hmp.txt' to denote chromosome and section (required)
    -mxDiv <Max divergence from founder> : Maximum genetic divergence from founder haplotype to cluster sequences (Default: 0.01)
    -mxHet <Max heterozygosity of output haplotypes> : Maximum heterozygosity of output haplotype. Heterozygosity results from clustering sequences that either have residual heterozygosity or clustering sequences that do not share all minor alleles. (Default: 0.01)
    -minSites <Min sites to cluster> : The minimum number of sites present in two taxa to compare genetic distance to evaluate similarity for clustering (Default: 50)
    -mxErr <Max combined error to impute two donors> : The maximum genetic divergence allowable to cluster taxa (Default: 0.05)
    -hapSize <Preferred haplotype size> : Preferred haplotype block size in sites (minimum 64); will use the closest multiple of 64 at or below the supplied value (Default: 8192)
    -minPres <Min sites to test match> : Minimum number of present sites within input sequence to do the search (Default: 500)
    -maxHap <Max haplotypes per segment> : Maximum number of haplotypes per segment (Default: 3000)
    -minTaxa <Min taxa to generate a haplotype> : Minimum number of taxa to generate a haplotype (Default: 2)
    -maxOutMiss <Max frequency missing per haplotype> : Maximum frequency of missing data in the output haplotype (Default: 0.4)
    -nV <true | false> : Supress system out (Default: false)
    -extOut <true | false> : Details of taxa included in each haplotype written to file (Default: false)


    ${tasselPath} ${Xms} ${Xmx} -fork1 -FILLINImputationPlugin \
    -hmp <Target file> \
    -d <Donor> \
    -o <Output filename> \
    -hapSize 8000 \
    -mxHet 0.02 \
    -mxInbErr 0.01 \
    -mxHybErr 0.003 \
    -mnTestSite 20 \
    -minMnCnt 20 \
    -mxDonH 20 \
    -accuracy false \
    -propSitesMask 0.01 \
    -depthMask 9 \
    -propDepthSitesMask 0.2 \
    -endPlugin \
    -runfork1 >> ${logPath} \
    2>&1 | tee -a ${logPath}


    

Usage:
FILLINImputationPlugin <options>
    -hmp <Target file> : Input HapMap file of target genotypes to impute. Accepts all file types supported by TASSEL5. This file should include _ALL_ available sites, not just those segregating in your material. (ie: don't filter the input) (required)
    -d <Donor> : Directory containing donor haplotype files from output of FILLINFindHaplotypesPlugin. All files with '.gc' in the filename will be read in, only those with matching sites are used. Alternately, a single file to use as a donor, will be cut into sub genos in size specified (eg, high densitySNP file for projection (required)
    -o <Output filename> : Output file; hmp.txt.gz and .hmp.h5 accepted. (required)
    -hapSize <Preferred haplotype size> : Preferred haplotype block size in sites (use same as in FILLINFindHaplotypesPlugin) (Default: 8000)
    -mxHet <Heterozygosity threshold> : Threshold per taxon heterozygosity for treating taxon as heterozygous (no Viterbi, het thresholds). (Default: 0.02)
    -mxInbErr <Max error to impute one donor> : Maximum error rate for applying one haplotype to entire site window (Default: 0.01)
    -mxHybErr <Max combined error to impute two donors> : Maximum error rate for applying Viterbi with two haplotypes to entire site window (Default: 0.003)
    -mnTestSite <Min sites to test match> : Minimum number of sites to test for IBS between haplotype and target in focus block (Default: 20)
    -minMnCnt <Min num of minor alleles to compare> : Minimum number of informative minor alleles in the search window (or 10X major) (Default: 20)
    -mxDonH <Max donor hypotheses> : Maximum number of donor hypotheses to be explored. (Note: For heterozygous samples you may want to set this number higher, but the computation load goes up quickly.) (Default: 20)
    -impAllHets <true | false> : Write all imputed heterozygous calls as such, even if the original file has a homozygous call. (Not recommended for inbred lines.) (Default: false)
    -hybNN <true | false> : If true, uses combination mode in focus block, else does not impute (Default: true)
    -ProjA <true | false> : Create a projection alignment for high density markers (Default: false)
    -impDonor <true | false> : Impute the donor file itself (Default: false)
    -nV <true | false> : Supress system out (Default: false)
    -accuracy <true | false> : Masks input file before imputation and calculates accuracy based on masked genotypes (Default: false)
    -propSitesMask <Proportion of genotypes to mask if no depth> : Proportion of genotypes to mask for accuracy calculation if depth not available (Default: 0.01)
    -depthMask <Depth of genotypes to mask> : Depth of genotypes to mask for accuracy calculation if depth information available (Default: 9)
    -propDepthSitesMask <Proportion of depth genotypes to mask> : Proportion of genotypes of given depth to mask for accuracy calculation if depth available (Default: 0.2)
    -maskKey <Optional key to calculate accuracy> : Key to calculate accuracy. Genotypes missing (masked) in target file should be present in key, with all other sites set to missing. Overrides otheraccuracy options if present and all sites and taxa present in target file present
    -byMAF <true | false> : Calculate R2 accuracy within MAF categories based on donor file (Default: false)












